import java.util.Scanner;
public class Assignment2{

	public static void main(String[] args)
{	
	Scanner keyboard = new Scanner(System.in);
	//named scanner variable to keyboard for system input
	System.out.println("Enter first number");
	//prompt to enter 1 of 5 digits
	double num1 = keyboard.nextDouble();
	//scanner takes in next double entered by user
	System.out.println(num1);
	//displays number for confirmation
	System.out.println("Enter second number");
	double num2 = keyboard.nextDouble();
	System.out.println(num2);
	System.out.println("Enter third number");
	double num3 = keyboard.nextDouble();
	System.out.println(num3);
	System.out.println("Enter fourth number");
	double num4 = keyboard.nextDouble();
	System.out.println(num4);
	System.out.println("Enter fifth number");
	double num5 = keyboard.nextDouble();
	System.out.println(num5);
	System.out.printf("The five numbers you have entered are: %.2f, %.2f, %.2f, %.2f, %.2f", num1, num2, num3, num4, num5);
	//displays all 5 numbers using .2f for floating decimal up to two places
	double sum;
	sum = num1+num2+num3+num4+num5;
	double average;
	average = sum/5;
	//divided the average by 5 (max number if inputs)
	System.out.println("The sum of those numbers is equal to " + sum);
	System.out.println("The average is " + average);
	}

}